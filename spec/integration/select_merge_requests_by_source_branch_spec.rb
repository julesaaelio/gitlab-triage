# frozen_string_literal: true

require 'spec_helper'

describe 'select merge requests by source_branch' do
  include_context 'with integration context'

  describe 'source_branch' do
    let!(:mr_from_feature_1) do
      mr.merge(source_branch: 'feature-1')
    end
    let!(:mr_from_feature_2) do
      mr.merge(source_branch: 'feature-2', iid: issue[:iid] * 2)
    end

    before do
      stub_api(
        :get,
        "https://gitlab.com/api/v4/projects/#{project_id}/merge_requests",
        query: { per_page: 100, source_branch: 'feature-2' },
        headers: { 'PRIVATE-TOKEN' => token }) do
        [mr_from_feature_2]
      end
    end

    it 'comments on the merge request' do
      rule = <<~YAML
        resource_rules:
          merge_requests:
            rules:
              - name: Rule name
                conditions:
                  source_branch: 'feature-2'
                actions:
                  comment: |
                    Comment because its source branch is `{{source_branch}}`.
      YAML

      stub_post_mr_from_feature_2 = stub_api(
        :post,
        "https://gitlab.com/api/v4/projects/#{project_id}/merge_requests/#{mr_from_feature_2[:iid]}/notes",
        body: { body: 'Comment because its source branch is `feature-2`.' },
        headers: { 'PRIVATE-TOKEN' => token })

      perform(rule)

      assert_requested(stub_post_mr_from_feature_2)
    end
  end
end
